package ro.orangeStartIT;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        // implement receptionists
        ArrayList receptionists = new ArrayList<String>(5); //declare an array of Strings, allocate memory for 5 strings;
        ArrayList floor = new ArrayList<Integer>(5);

        receptionists.add("John Snow");
        receptionists.add("William Cooper");
        receptionists.add("Patricia Johnson");
        receptionists.add("Kate Anderson");
        receptionists.add("Dustin Rhodes");

        int total = receptionists.size();
//        System.out.println(total);

        for (int contor = 0; contor < total; contor++) {
//        System.out.println(contor);
            System.out.println("The receptionist on the " + contor + " floor is: " + receptionists.get(contor).toString());
        }

        //part 2
        String Owners[][] = new String[2][2];
        Owners[0][0] = "Mr";
        Owners[0][1] = "Smith Riley";
        Owners[1][0] = "Mrs";
        Owners[1][1] = "Catherine Jade";
        System.out.println(Owners[0][0] + " " + Owners[0][1] + " and " + Owners[1][0] + " " + Owners[1][1] + " are the owners of Plaza Hotel");

        //part 3

        char[] CharElem = {'B', 'E', 'D', 'O', 'N', 'A', 'L','D', 'A', 'T', 'E'};
        char[] Sec_guy = new char[6];
        System.arraycopy(CharElem, 2, Sec_guy, 0, 6);
        System.out.println("The hotel's security is provided by: " + new String(Sec_guy));

    }
}
